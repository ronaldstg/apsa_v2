angular.module('app.search', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('app.search-saved', {
                url: '/search/saved',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'app/search/search-saved.html'
                    }
                },
                controller: 'SearchSavedCtrl'
            })
            .state('app.search-index', {
                url: '/search/index',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'app/search/search.html'
                    }
                },
                controller: 'SearchIndexCtrl'
            })
            .state('app.search-map', {
                url: '/search/map',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'app/search/map-search.html'
                    }
                },
                controller: 'SearchMapCtrl'
            })
            .state('app.search-result', {
                url: '/search/result',
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: 'app/search/result.html'
                    }
                },
                controller: 'SearchResultsCtrl'
            });
    })

    .controller('SearchIndexCtrl', function ($scope, $state, $ionicLoading, FilterService, Location) {

        $ionicLoading.show({
            content: 'Carregando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        /** Recebendo informações do webservice da Apsa, e preenchendo os campos **/
        var servicesDataPromise = FilterService.loadData();
        //var filter = {};

        $scope.filter = FilterService.filter();


        servicesDataPromise.then(function (data) {

            $scope.filter.realtyTypes = FilterService.getRealtyTypes();
            $scope.filter.selectedRealtyType = $scope.filter.realtyTypes[0];

            $scope.filter.realtyPrices = FilterService.getRealtyPrices();
            var realtyPricesSize = $scope.filter.realtyPrices.length - 1;

            $scope.filter.selectedMinRealtyPrice = $scope.filter.realtyPrices[0];
            $scope.filter.selectedMaxRealtyPrice = $scope.filter.realtyPrices[realtyPricesSize];


            $scope.filter.areaSizes = FilterService.getAreaSizes();
            $scope.filter.selectedRealtyMinArea = $scope.filter.areaSizes[0];
            $scope.filter.selectedRealtyMaxArea = $scope.filter.areaSizes[0];


            $scope.filter.orderByOptions = FilterService.getOrderByOptions();
            $scope.filter.selectedOrderBy = $scope.filter.orderByOptions[0];

            $scope.filter.rooms = FilterService.getRoomOptions();
            $scope.filter.selectedRoom = $scope.filter.rooms[0];

            $scope.filter.spots = FilterService.getSpotsOptions();
            $scope.filter.selectedSpot = $scope.filter.spots[0];

            $scope.filter.realtyCode = '';


            /* After load all Promises */
            $ionicLoading.hide();

        });

        $scope.doRealtySearch = function () {
            $state.go('app.search-result');
        };


    })
    .controller('SearchResultsCtrl', function ($scope, $state, $stateParams, $ionicLoading, $ionicHistory, $ionicModal, FilterService, Search, Favorites, Searches) {

        $scope.favorites = Favorites.all();
        $scope.savedSearches = Searches.all();

        var favidsArr = Favorites.favids();

        var responseArr = [];
        var i = 0;

        var filter = FilterService.filter();
        var address = FilterService.address();


        //TODO COMPLETAR
        var mySearch = {
            TipoImovel: filter.selectedRealtyType.Codigo,
            TipoImovelDescricao: filter.selectedRealtyType.Descricao,
            Bairro: address.bairro,
            Cidade: address.cidade,
            UF: address.uf,
            Full: address.bairro + ', ' + address.cidade + '/' + address.uf,
        };


        if (filter.realtyCode != '') {

            Search.setMethod(filter.selectedSearchType.value == 0 ? 'BuscarImoveisAluguelPorCodigo' : 'BuscarImoveisVendaPorCodigo');
            Search.setQuery({CodigoImovel: filter.realtyCode, UF: address.full});

            mySearch.CodigoImovel = filter.realtyCode;


        } else {
            Search.setMethod('BuscarImoveisAluguel');

            var res = address.full.split(",");

            Search.setQuery({
                TipoImovel: filter.selectedRealtyType.Codigo,
                Bairro: res[0],
                Cidade: res[1].trim(),
                UF: res[2].trim()
            });

            console.log('Query: ' + JSON.stringify({
                    TipoImovel: filter.selectedRealtyType.Codigo,
                    Bairro: res[0],
                    Cidade: res[1].trim(),
                    UF: res[2].trim()
                }));

        }

        var promise1 = Search.loadData();

        $ionicHistory.clearCache();
        $scope.results = [];
        $scope.filter = FilterService.filter();

        $ionicLoading.show({
            content: 'Carregando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        $ionicModal.fromTemplateUrl('templates/modalNotFound.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        promise1.then(function () {

            responseArr = Search.result();

            $ionicLoading.hide();
            $scope.populateList();
            $scope.searchResult = responseArr.length;

        }, function () {
            $ionicLoading.hide();
            $scope.modal.show();

        });

        $scope.populateList = function () {

            for (c = 0; c < responseArr.length; c++) {

                var _code = responseArr[i].CodigoImovel;
                var _isFavorited = (favidsArr.indexOf(_code) > -1) ? true : false

                var _address = responseArr[i].Endereco;

                $scope.results.push({
                    id: i,
                    code: _code,
                    title: _address.TipoLogradouro + ' ' + _address.Logradouro + ' - ' + _address.Bairro,
                    price: responseArr[i].PrecoLocacao,
                    realtyType: responseArr[i].TipoImovel,
                    area: responseArr[i].AreaUtil,
                    dorms: responseArr[i].QtdDormitorios,
                    suites: '',
                    spots: responseArr[i].QtdVagas,
                    cond: responseArr[i].PrecoCondominio,
                    photos: responseArr[i].Fotos,
                    isFavorited: _isFavorited
                });

                i = i + 1;
            }

            //console.log('RESULTS: ' + JSON.stringify(responseArr));

            $scope.$broadcast('scroll.infiniteScrollComplete');
        }

        $scope.canWeLoadMoreContent = function () {
            return (i >= responseArr.length) ? false : true;
        }

        $scope.goDetails = function (_realtyCode) {
            $state.go('app.realty-index', {'realtyCode': _realtyCode});
        };


        $scope.saveSearch = function () {

            $scope.savedSearches.push(mySearch);
            Searches.save($scope.savedSearches);
        };


    })
    .controller('SearchMapCtrl', function ($scope, $state, $ionicLoading,GoogleMaps, Location,Search, FilterService) {

        var filter = FilterService.filter();
        var googleMaps;

        $ionicLoading.show({
            content: 'Carregando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        $scope.range = 250;

       /* $scope.$watch('range', function (value) {

            if (circle) {
                circle.set('radius', parseInt(value, 10));
                //showMarkersInArea();
            }


        }, true);*/

        //var overlay;
        //var points = [];
        //var circle;
        //var circleBounds;
        //var arrMarkers = new Array(0); // array com os pontos representando os imóveis
        //var myLatlng;
        //var iw = null;


        //google.maps.Circle.prototype.contains = function(latLng) {
        //  return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
        //}


        //var lat = position.coords.latitude;
        //var long = position.coords.longitude;

        //var googleMaps = new GoogleMaps(lat,long);

        var location = new Location();
        location.getPosition().then(function() {

            var lat = location.position.latitude;
            var long = location.position.longitude;

            googleMaps = new GoogleMaps(lat,long);

            location.getReverse(lat,long).then(function(results) {
                var arrAddress = results[0].address_components;
                var bairro;
                var cidade;
                var uf;


                console.log(JSON.stringify(arrAddress));


                angular.forEach(arrAddress, function (item, pos) {


                    if (item.types.indexOf('sublocality_level_1') > -1) // Bairro
                        bairro = item.long_name;


                    if (item.types.indexOf('administrative_area_level_2') > -1) // Cidade
                        cidade = item.long_name;


                    if (item.types.indexOf('administrative_area_level_1') > -1) // UF
                        uf = item.short_name;


                });

                 Search.setMethod('BuscarImoveisAluguel');
                 Search.setQuery({
                     Bairro: bairro.trim(),
                     Cidade: cidade.trim(),
                     UF: uf.trim()
                 });


                 Search.loadData().then(function(){

                     responseArr = Search.result();
                     points =  [];

                     for (i = 0; i < responseArr.length; i++) {

                         coord = responseArr[i].CoordenadasGeograficas;
                         point = new google.maps.LatLng(coord.Latitude, coord.Longitude);

                         //console.log('coord: ' + JSON.stringify(coord));

                         points.push(point);

                     }

                     points.push(new google.maps.LatLng(-23.002976, -43.315772));
                     points.push(new google.maps.LatLng(-23.006067, -43.317199));

                     googleMaps.placeRealties(points);


                     $ionicLoading.hide();

                 });


            });



        });


    })
    .controller('SearchSavedCtrl', function ($scope, Searches) {

        $scope.savedSearches = Searches.all();

        /*$scope.savedSearches= [
         {id: 0, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 1, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 2, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 3, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 4, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 5, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 6, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 7, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 8, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 9, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 10, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 11, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'},
         {id: 12, title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', propertyType: 'Apartamento', price: '200.000', rooms: '2', spots: '1', area: '82'}
         ]; */

        console.warn('Searches: ' + JSON.stringify(Searches.all()))

    });























