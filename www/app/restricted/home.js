angular.module('app.restricted.home', [])

    .config(function ($ionicConfigProvider, $stateProvider) {

        $stateProvider
            .state('app.restricted-home', {
                url: '/restricted/home',
                cache:false,
                views: {
                    'menuContent': {
                        templateUrl: 'app/restricted/home.html',
                    }
                },
                controller:'RestrictedHomeCtrl'
            })
    })

    .controller('RestrictedHomeCtrl', function($scope, $state, $ionicLoading, UserService) {

        /*$ionicLoading.show({
            content: 'Carregando',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });*/



    })